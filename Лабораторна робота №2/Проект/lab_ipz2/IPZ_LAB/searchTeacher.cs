﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPZ_LAB
{
    public partial class searchTeacher : Form
    {
        public searchTeacher()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Transitional transitional = new Transitional();
            transitional.ShowDialog();
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> items = new List<string>();
            switch (comboBox3.SelectedIndex)
            {
                case 0:
                    comboBox4.Items.Clear();
                    items.Clear();
                    items.Add("EOM");
                    items.Add("CKC");
                    comboBox4.Items.AddRange(items.ToArray());
                    break;
                case 1:
                    comboBox4.Items.Clear();
                    items.Clear();
                    items.Add("ISM");
                    items.Add("ASU");
                    comboBox4.Items.AddRange(items.ToArray());
                    break;
                case 2:
                    comboBox4.Items.Clear();
                    items.Clear();
                    items.Add("EP");
                    items.Add("TK");
                    comboBox4.Items.AddRange(items.ToArray());
                    break;
                case 3:
                    comboBox4.Items.Clear();
                    items.Clear();
                    items.Add("ADM");
                    items.Add("BKM");
                    comboBox4.Items.AddRange(items.ToArray());
                    break;
            }

        }

        private void regbtn_Click(object sender, EventArgs e)
        {
            try { 
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Socket socket1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Socket socket2 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect("127.0.0.1", 904);
            socket1.Connect("127.0.0.1", 905);
            socket2.Connect("127.0.0.1", 906);

            string direct = comboBox3.Text.TrimEnd('\0');
            string direct1 = comboBox4.Text.TrimEnd('\0');
            string direct2 = textBox1.Text.TrimEnd('\0');
            string direct3 = textBox2.Text.TrimEnd('\0');
            string direct4 = textBox3.Text.TrimEnd('\0');
            string query = $"select * from Teacher1 where Prizv like '%{direct2}%'";
            if (direct != null || direct != "") query += $" and Instytut  like '%{direct}%'";
            if (direct1 != null || direct1 != "") query += $" and Kafedra  like '%{direct1}%'";
            if (direct3 != null || direct3 != "") query += $" and Imya  like '%{direct3}%'";
            if (direct4 != null || direct4 != "") query += $" and Pobatkovi  like '%{direct4}%'";


            string input = "3" + Char.MinValue;

            byte[] buffer = Encoding.ASCII.GetBytes(query);
            socket.Send(buffer, buffer.Length, 0);

            byte[] buffer2 = Encoding.ASCII.GetBytes(input);
            socket2.Send(buffer2, buffer2.Length, 0);

            buffer = new byte[1024];

            buffer2 = new byte[1024];
            socket.Receive(buffer);
            string tmp = "" + Char.MinValue;
            tmp = Encoding.ASCII.GetString(buffer);
            string newtmp = tmp.TrimEnd('\0');
            string[] teachersArr = newtmp.Split('/');

            foreach (var teacher in teachersArr)
            {
                Викладачі.Items.Add(teacher.Trim('/'));
            }
                //Викладачі.Items.Add(newtmp);
            }
            catch
            {
                MessageBox.Show("Відсутнє з'єднання з сервером");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 'A' && e.KeyChar <= 'Z') || (e.KeyChar >= 'a' && e.KeyChar <= 'z') || e.KeyChar == (char)Keys.Back)
            {

            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Лише [A-z]");

            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 'A' && e.KeyChar <= 'Z') || (e.KeyChar >= 'a' && e.KeyChar <= 'z') || e.KeyChar == (char)Keys.Back)
            {

            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Лише [A-z]");

            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 'A' && e.KeyChar <= 'Z') || (e.KeyChar >= 'a' && e.KeyChar <= 'z') || e.KeyChar == (char)Keys.Back)
            {

            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Лише [A-z]");

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Викладачі.Items.Clear();
        }
    }
}

