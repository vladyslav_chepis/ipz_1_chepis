﻿
namespace lab_ipz2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainloginpanel = new System.Windows.Forms.Panel();
            this.findDbtn = new System.Windows.Forms.Button();
            this.findTbtn = new System.Windows.Forms.Button();
            this.loginpanel = new System.Windows.Forms.Panel();
            this.closebtn = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mainloginpanel.SuspendLayout();
            this.loginpanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainloginpanel
            // 
            this.mainloginpanel.BackColor = System.Drawing.Color.Yellow;
            this.mainloginpanel.Controls.Add(this.findDbtn);
            this.mainloginpanel.Controls.Add(this.findTbtn);
            this.mainloginpanel.Controls.Add(this.loginpanel);
            this.mainloginpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainloginpanel.Location = new System.Drawing.Point(0, 0);
            this.mainloginpanel.Name = "mainloginpanel";
            this.mainloginpanel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.mainloginpanel.Size = new System.Drawing.Size(379, 278);
            this.mainloginpanel.TabIndex = 2;
            // 
            // findDbtn
            // 
            this.findDbtn.BackColor = System.Drawing.Color.Yellow;
            this.findDbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.findDbtn.Location = new System.Drawing.Point(206, 80);
            this.findDbtn.Name = "findDbtn";
            this.findDbtn.Size = new System.Drawing.Size(151, 195);
            this.findDbtn.TabIndex = 2;
            this.findDbtn.Text = "Пошук дисципліни";
            this.findDbtn.UseVisualStyleBackColor = false;
            this.findDbtn.Click += new System.EventHandler(this.findDbtn_Click);
            // 
            // findTbtn
            // 
            this.findTbtn.BackColor = System.Drawing.Color.Yellow;
            this.findTbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.findTbtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.findTbtn.Location = new System.Drawing.Point(22, 80);
            this.findTbtn.Name = "findTbtn";
            this.findTbtn.Size = new System.Drawing.Size(151, 195);
            this.findTbtn.TabIndex = 1;
            this.findTbtn.Text = "Пошук викладача";
            this.findTbtn.UseVisualStyleBackColor = false;
            this.findTbtn.Click += new System.EventHandler(this.findTbtn_Click);
            // 
            // loginpanel
            // 
            this.loginpanel.BackColor = System.Drawing.Color.DarkBlue;
            this.loginpanel.Controls.Add(this.closebtn);
            this.loginpanel.Controls.Add(this.label1);
            this.loginpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.loginpanel.Location = new System.Drawing.Point(0, 0);
            this.loginpanel.Name = "loginpanel";
            this.loginpanel.Size = new System.Drawing.Size(379, 74);
            this.loginpanel.TabIndex = 0;
            this.loginpanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.loginpanel_MouseDown);
            this.loginpanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.loginpanel_MouseMove);
            // 
            // closebtn
            // 
            this.closebtn.AutoSize = true;
            this.closebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closebtn.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closebtn.ForeColor = System.Drawing.Color.White;
            this.closebtn.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.closebtn.Location = new System.Drawing.Point(356, 0);
            this.closebtn.Name = "closebtn";
            this.closebtn.Size = new System.Drawing.Size(20, 19);
            this.closebtn.TabIndex = 1;
            this.closebtn.Text = "X";
            this.closebtn.Click += new System.EventHandler(this.closebtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 23F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label1.Location = new System.Drawing.Point(85, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "Оберіть дію";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 278);
            this.Controls.Add(this.mainloginpanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.mainloginpanel.ResumeLayout(false);
            this.loginpanel.ResumeLayout(false);
            this.loginpanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel mainloginpanel;
        private System.Windows.Forms.Panel loginpanel;
        private System.Windows.Forms.Label closebtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button findTbtn;
        private System.Windows.Forms.Button findDbtn;
    }
}