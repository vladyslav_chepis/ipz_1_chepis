﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_ipz2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void closebtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void findTbtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            searchTeacher searchteacher = new searchTeacher();
            searchteacher.Show();
        }

        private void findDbtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            searchDiscipline searchdiscipline = new searchDiscipline();
            searchdiscipline.Show();
                }
        Point lastPoint;
        private void loginpanel_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void loginpanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
    }
}
